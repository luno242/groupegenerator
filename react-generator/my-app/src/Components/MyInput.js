import React, { Component } from 'react';
import axios from 'axios';

class MyInput extends Component {
    constructor(props) {
        super(props);
        this.state = { value: '' };
        this.handleChange = this.handleChange.bind(this);       
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({ value: event.target.value });
    }
    handleSubmit(event) {
        // alert('A name was submitted: ' + this.state.value);
        event.preventDefault();
        const firstname= {
            firstname: this.state.value};
       // console.log(JSON.stringify({ firstname: this.state.value }));
      /*  fetch('http://localhost:3001/new/user', {
            method: 'post',
            // headers: { 'Content-Type': 'application/json' },
            headers: new Headers(),
            body: JSON.stringify({firstname: this.state.value})
        }).then((res) => res.json())
            .then((data) => console.log(data))
            .catch((err) => console.log(err))*/
        axios.post(`http://localhost:3001/new/user`, firstname)
            .then(res => {
                console.log(res);
                console.log(res.data);
            })
    };

    render() {
        return (

            <div id="signup">
                <form onSubmit={this.handleSubmit} className="form-inline" >
                    <label className="sr-only" for="inlineFormInputName2">Name</label>
                    <input value={this.state.value} onChange={this.handleChange}
                                            placeholder="Entre un nom"
                                            type="text" name="firstname"
                                            className="form-control mb-2 mr-sm-2"
                                            id="inlineFormInputName2" />
                    <label className="sr-only" for="inlineFormInputGroupUsername2">Username</label>
                    <button type="submit" value="Submit" className="btn btn-secondary" >+</button>
                </form>
            </div>
        );
    }
}

export default MyInput;