import React, { Component } from 'react';

class MyUsers extends Component {
    constructor(props) {
        super(props);
        this.state = { apiResponse: " "};
    }

    callAPI() {

        fetch("http://localhost:3001/userlist")
            .then((res) => { return res.json() })
            .then(res => this.setState({ apiResponse: res }));
    }

    componentWillMount() {
        this.callAPI();
    }

    render() {
        return (
            < table className="table table-striped" >
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                    </tr>
                </thead>
                <tbody>
                  
                    <tr>
                        <th scope="row">1</th>
                    <td>{this.state.apiResponse}</td>
                        
                    </tr>
                </tbody>
                    </table >
           
        );
    }
}

export default MyUsers;