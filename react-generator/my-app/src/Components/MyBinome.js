import React, { Component } from 'react';

class MyBinome extends Component {

    render() {
        return (
            <div className="binom">
                <button type="submit" type="button" className="btn btn-secondary">∞</button>
            
            <table className="table table-striped" >
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name 1</th>
                        <th scope="col">Name 2</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td></td>
                        <td></td>

                    </tr>
                </tbody>
            </table >
            </div>
        );
    }
}

export default MyBinome;