import React, { Component } from 'react';
//import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './App.css';
import MyInput from './Components/MyInput';
import MyUsers from './Components/MyUsers';
import MyBinome from './Components/MyBinome';

 class App extends Component {
  render() {
    return (
      <div className="App">
          <MyInput />
          <MyUsers />
          <MyBinome />
      </div>
    );
  }
}
export default App;

